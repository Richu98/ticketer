# Ticketer

Project Execution for Public Bus Transportation ( Higher view of Implementation )

1. A ticketing service which can be used for all public transportation where the Travelcards will be allocated based on the Aadhaar card(Government ID Card) which has already data of individuals like Face data and retina scan which can be used to identify people uniquely.

2. A Device which has camera will be installed at both the entry and exit gate to recognise the people in the images.

3. A software to calculate the distance travel by them from the point of entry to the exit.

